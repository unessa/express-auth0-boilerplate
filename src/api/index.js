// @flow

import apicache from 'apicache'

import { configureApiAuthentication } from '../middleware/api-authentication'
import { configureRoutesHealthCheck, API_PATH_HEALTH_CHECK } from './v1/health'
import { configureRoutesAuth, API_PATH_AUTH_LOGIN } from './v1/auth'
import { configureRoutesImdb, API_PATH_IMDB } from './v1/imdb'

import type { $Application } from 'express'
import type { ServerConfig } from '../config'
import type { RedisClient } from '../utils/redis-client'

export async function configureApiRoutes(
  app: $Application,
  config: ServerConfig,
  userTokenWhitelistRedis: RedisClient,
  apicacheRedis: RedisClient
): Promise<void> {
  const authIgnoredPaths: Array<string> = [
    API_PATH_HEALTH_CHECK,
    API_PATH_AUTH_LOGIN,
    API_PATH_IMDB
  ]

  await configureApiAuthentication(app, config, authIgnoredPaths, userTokenWhitelistRedis)

  const apicacheMiddleware = configureApicacheMiddleware(config, apicacheRedis)

  configureRoutesHealthCheck(app, config, userTokenWhitelistRedis)
  configureRoutesAuth(app, config, userTokenWhitelistRedis)
  configureRoutesImdb(app, config, apicacheMiddleware)
}

function configureApicacheMiddleware(
  config: ServerConfig,
  apicacheRedis: RedisClient
): Function {
  return apicache.options({
    enabled: config.apicache.enabled,
    redisClient: apicacheRedis,
    statusCodes: {
      include: config.apicache.includedStatusCodes
    }
  }).middleware(config.apicache.expires)
}
