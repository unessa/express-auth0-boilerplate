// @flow

import {
  HEALTH_CHECK_MESSAGE_SERVICE_IS_HEALTHY,
  HEALTH_CHECK_MESSAGE_REDIS_NOT_CONNECTED
} from './health'

import type { MockResponse } from '../../../test-utils'

export const serviceHealthSuccessResponse: MockResponse = {
  status: 200,
  body: {
    message: HEALTH_CHECK_MESSAGE_SERVICE_IS_HEALTHY
  }
}

export const redisConnectionFailedResponse: MockResponse = {
  status: 503,
  body: {
    message: HEALTH_CHECK_MESSAGE_REDIS_NOT_CONNECTED
  }
}
