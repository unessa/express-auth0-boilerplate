// @flow

import type { $Request, $Response } from 'express'
import type { ServerConfig } from '../../../config'
import type { RedisClient } from '../../../utils/redis-client'

export const HEALTH_CHECK_MESSAGE_SERVICE_IS_HEALTHY = 'OK'
export const HEALTH_CHECK_MESSAGE_REDIS_NOT_CONNECTED = 'Redis connection failed'

/**
 * @api {get} /api/v1/health
 * @apiDescription Checks service status
 * @apiName Server
 * @apiGroup health
 * @apiVersion 1.0.0
 *
 * @apiSuccess {Object} 200 OK
 */
export function getServiceHealthRequest(
  config: ServerConfig,
  userTokenWhitelistRedis: RedisClient
) {
  return async (req: $Request, res: $Response) => {
    const isRedisHealthy = userTokenWhitelistRedis.connected
    const serviceStatus: number = isRedisHealthy ? 200 : 503
    const message = isRedisHealthy ? HEALTH_CHECK_MESSAGE_SERVICE_IS_HEALTHY : HEALTH_CHECK_MESSAGE_REDIS_NOT_CONNECTED

    res.status(serviceStatus).send({ message })
  }
}
