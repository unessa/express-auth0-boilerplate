// @flow

import { getServiceHealthRequest } from './health'

import type { $Application } from 'express'
import type { ServerConfig } from '../../../config'
import type { RedisClient } from '../../../utils/redis-client'

export const API_PATH_HEALTH_CHECK = '/api/v1/health'

export function configureRoutesHealthCheck(
  app: $Application,
  config: ServerConfig,
  userTokenWhitelistRedis: RedisClient
): void {
  app.get(API_PATH_HEALTH_CHECK, getServiceHealthRequest(config, userTokenWhitelistRedis))
}
