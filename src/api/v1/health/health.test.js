// @flow

import {
  startServer,
  stopServer,
  fakeUserTokenWhitelistRedis,
  untokenizedApiRequest,
  expectApiResponse
} from '../../../test-utils'

import { API_PATH_HEALTH_CHECK } from '.'

import {
  serviceHealthSuccessResponse,
  redisConnectionFailedResponse
} from './health.mock'

describe('health check', () => {
  beforeEach(startServer)
  afterEach(stopServer)

  it('GET 200 - Health check return ok status', async () => {
    const res = await untokenizedApiRequest(API_PATH_HEALTH_CHECK).get()
    expectApiResponse(res, serviceHealthSuccessResponse)
  })

  it('GET 503 - Redis client is not available', async () => {
    fakeUserTokenWhitelistRedis.connected = false
    const res = await untokenizedApiRequest(API_PATH_HEALTH_CHECK).get()
    expectApiResponse(res, redisConnectionFailedResponse)
  })
})
