// @flow

import { imdbRequest } from './imdb'

import type { $Application } from 'express'
import type { ServerConfig } from '../../../config'

export const API_PATH_IMDB = '/api/v1/imdb'

export function configureRoutesImdb(
  app: $Application,
  config: ServerConfig,
  apicacheMiddleware: Function
): void {
  app.get(API_PATH_IMDB, apicacheMiddleware, imdbRequest(config))
}
