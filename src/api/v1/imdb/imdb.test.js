// @flow

import config from '../../../config'
import { imdbMovieTitle, imdSuccessResponse } from './imdb.mock'
import { API_PATH_IMDB } from '.'

import {
  startServer,
  stopServer,
  untokenizedApiRequest,
  expectApiResponse,
  mockServiceRequest
} from '../../../test-utils'

describe('IMDB API tests', () => {
  beforeAll(startServer)
  afterAll(stopServer)

  it.only('GET 200 - fetch movie information from imdb', async () => {
    mockServiceRequest(config.omdbApiUri, `/?t=${imdbMovieTitle}`).get(imdSuccessResponse)
    const res = await untokenizedApiRequest(`${API_PATH_IMDB}?title=${imdbMovieTitle}`).get()
    expectApiResponse(res, imdSuccessResponse)
  })
})
