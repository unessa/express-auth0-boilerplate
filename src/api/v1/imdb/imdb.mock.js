// @flow

import type { MockResponse } from '../../../test-utils'

export const imdbMovieTitle = 'Casablanca'

export const imdSuccessResponse: MockResponse = {
  status: 200,
  body: {
    Title: imdbMovieTitle,
    Year: '1942',
    Rated: 'PG',
    Released: '23 Jan 1943',
    Runtime: '102 min',
    Genre: 'Drama, Romance, War',
    Director: 'Michael Curtiz',
    Writer: 'Julius J. Epstein (screenplay), Philip G. Epstein (screenplay), Howard Koch (screenplay), Murray Burnett (play), Joan Alison (play)',
    Actors: 'Humphrey Bogart, Ingrid Bergman, Paul Henreid, Claude Rains',
    Plot: 'In Casablanca in December 1941, a cynical American expatriate encounters a former lover, with unforeseen complications.',
    Language: 'English, French, German, Italian',
    Country: 'USA',
    Awards: 'Won 3 Oscars. Another 4 wins & 8 nominations.',
    Poster: 'https://images-na.ssl-images-amazon.com/images/M/MV5BY2IzZGY2YmEtYzljNS00NTM5LTgwMzUtMzM1NjQ4NGI0OTk0XkEyXkFqcGdeQXVyNDYyMDk5MTU@._V1_SX300.jpg',
    Ratings: [{
      Source: 'Internet Movie Database',
      Value: '8.5/10'
    }, {
      Source: 'Rotten Tomatoes',
      Value: '97%'
    }],
    Metascore: 'N/A',
    imdbRating: '8.5',
    imdbVotes: '409,719',
    imdbID: 'tt0034583',
    Type: 'movie',
    DVD: '15 Feb 2000',
    BoxOffice: 'N/A',
    Production: 'Warner Bros. Pictures',
    Website: 'http://www.FathomEvents.com',
    Response: 'True'
  }
}
