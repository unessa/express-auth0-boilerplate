// @flow

import { externalRequest } from '../../../utils/external-request'

import type { $Request, $Response, NextFunction } from 'express'
import type { ServerConfig } from '../../../config'

/**
 * @api {get} /api/v1/imdb?title=:title
 * @apiParam {String} title Title to be searched
 * @apiParam {Boolean} episodes Include episodes
 * @apiDescription Fetch movie information from imdb
 * @apiName imdb
 * @apiGroup misc
 * @apiVersion 1.0.0
 *
 * @apiSuccess (200) {Object} movie data
 *
 * @apiExample {curl} curl
 *   curl http://localhost:3000/api/v1/imdb?title=Casablanca
 */
export function imdbRequest(
  config: ServerConfig
): Function {
  return async (req: $Request, res: $Response, next: NextFunction): Promise<void> => {
    try {
      const query = `/?t=${req.query.title}`
      const omdbResponse = await externalRequest('GET', `${config.omdbApiUri}${query}`)
      res.status(200).send(omdbResponse)
    } catch (e) {
      next(e)
    }
  }
}
