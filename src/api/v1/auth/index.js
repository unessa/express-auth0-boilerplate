// @flow

import { ApiError } from '../../../utils/custom-errors'
import { loginRequest } from './login/login'
import { logoutRequest } from './logout/logout'

import type { $Application } from 'express'
import type { ServerConfig } from '../../../config'
import type { RedisClient } from '../../../utils/redis-client'

export const API_PATH_AUTH_LOGIN = '/api/v1/auth/login'
export const API_PATH_AUTH_LOGOUT = '/api/v1/auth/logout'

export function configureRoutesAuth(
  app: $Application,
  config: ServerConfig,
  userTokenWhitelistRedis: RedisClient
): void {
  app.post(API_PATH_AUTH_LOGIN, loginRequest(config, userTokenWhitelistRedis))
  app.get(API_PATH_AUTH_LOGOUT, logoutRequest(config, userTokenWhitelistRedis))
}

// TODO 20.04.2017 nviik - Move under service/auth0
export function resolveAuth0Error(e: Object) {
  if (e.statusCode && e.error.error && e.error.error_description) {
    return new ApiError(e.statusCode, e.error.error, e.error.error_description)
  } else {
    return e
  }
}
