// @flow

import {
  TEST_USER_EMAIL,
  TEST_USER_PASSWORD,
  type MockResponse,
  type TestJWTToken
} from '../../../../test-utils'

import { ERROR_SCHEMA_VALIDATION } from '../../../../utils/jsonschema-validator'

export const loginRequest: Object = {
  username: TEST_USER_EMAIL,
  password: TEST_USER_PASSWORD
}

export const invalidLoginRequest: Object = {
  username: 'invalid-email-address',
  password: TEST_USER_PASSWORD
}

export function createLoginSuccessResponse(testToken: TestJWTToken): MockResponse {
  return {
    status: 200,
    body: {
      token: testToken.token
    }
  }
}

export const loginInvalidUserPasswordResponse: MockResponse = {
  status: 401,
  body: {
    error: 'invalid_user_password',
    message: 'Wrong email or password.'
  }
}

export const jsonSchemaValidationErrorResponse: MockResponse = {
  status: 400,
  body: {
    error: ERROR_SCHEMA_VALIDATION,
    message: 'username does not conform to the "email" format'
  }
}
