// @flow

import { API_PATH_AUTH_LOGIN } from '../.'
import { isTokenWhitelisted } from '../../../../utils/user-token-whitelist'

import {
  startServer,
  stopServer,
  generateTestJWTToken,
  untokenizedApiRequest,
  expectApiResponse,
  fakeUserTokenWhitelistRedis,
  type TestJWTToken
} from '../../../../test-utils'

import {
  loginRequest,
  invalidLoginRequest,
  createLoginSuccessResponse,
  loginInvalidUserPasswordResponse,
  jsonSchemaValidationErrorResponse
} from './login.mock'

import {
  mockAuth0LoginSuccessRequest,
  mockAuth0LoginInvalidUserPasswordRequest
} from '../../../../service/auth0/login/login.mock'


describe('User login', () => {
  beforeAll(startServer)
  afterAll(stopServer)

  it('POST 200 - creates correct login info after successfull authentication', async () => {
    const testToken: TestJWTToken = generateTestJWTToken()
    mockAuth0LoginSuccessRequest(testToken)
    const res = await untokenizedApiRequest(API_PATH_AUTH_LOGIN).post(loginRequest)
    expectApiResponse(res, createLoginSuccessResponse(testToken))
    expect(await isTokenWhitelisted(fakeUserTokenWhitelistRedis, testToken.payload)).toBeTruthy()
  })

  it('POST 401 - Invalid username / password', async () => {
    const testToken: TestJWTToken = generateTestJWTToken()
    mockAuth0LoginInvalidUserPasswordRequest(testToken)
    const res = await untokenizedApiRequest(API_PATH_AUTH_LOGIN).post(loginRequest)
    expectApiResponse(res, loginInvalidUserPasswordResponse)
  })

  it('POST 400 - Invalid request body data', async () => {
    const res = await untokenizedApiRequest(API_PATH_AUTH_LOGIN).post(invalidLoginRequest)
    expectApiResponse(res, jsonSchemaValidationErrorResponse)
  })
})
