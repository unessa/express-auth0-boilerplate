// @flow

import { createAuth0LoginRequest, type Auth0LoginResponse } from '../../../../service'
import { validateJsonSchema } from '../../../../utils/jsonschema-validator'
import { whitelistUserToken } from '../../../../utils/user-token-whitelist'
import { decodeAuthToken, type DecodedAuthToken } from '../../../../utils/jwt-decode'
import { resolveAuth0Error } from '../index'

import type { $Response, NextFunction } from 'express'
import type { JsonSchema } from 'jsonschema'
import type { ServerConfig } from '../../../../config'
import type { RedisClient } from '../../../../utils/redis-client'

const loginJsonSchema: JsonSchema.JsonSchema = {
  id: 'LoginSchema',
  type: 'object',
  properties: {
    username: { type: 'string', format: 'email' },
    password: { type: 'string' }
  },
  required: [
    'username',
    'password'
  ]
}

/**
 * @api {post} /api/v1/auth/login
 * @apiParam {String} username
 * @apiParam {String} password
 * @apiDescription User login for using this api
 * @apiName Login
 * @apiGroup Auth
 * @apiVersion 1.0.0
 *
 * @apiSuccess {String} token JWT token
 *
 * @apiExample {curl} curl
 *   curl http://localhost:3000/api/v1/auth/login \
 *     -H "Content-Type: application/json" \
 *     -X POST \
 *     -d '{"username":"<INSERT_USERNAME>","password":"<INSERT_PASSWORD>"}'
 */
export function loginRequest(
  config: ServerConfig,
  userTokenWhitelistRedis: RedisClient
): Function {
  return async (req: any, res: $Response, next: NextFunction): Promise<void> => {
    try {
      validateJsonSchema(req.body, loginJsonSchema)

      const { username, password } = req.body
      const auth0LoginResponse: Auth0LoginResponse = await createAuth0LoginRequest(config, username, password)
      const authToken: DecodedAuthToken = decodeAuthToken(auth0LoginResponse.id_token, config.auth0.clientSecret)
      await whitelistUserToken(userTokenWhitelistRedis, authToken)

      res.status(200).send({
        token: auth0LoginResponse.id_token
      })
    } catch (e) {
      next(resolveAuth0Error(e))
    }
  }
}
