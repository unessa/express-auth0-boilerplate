// @flow

import type { MockResponse } from '../../../../test-utils'

export const LogoutSuccessResponse: MockResponse = {
  status: 200,
  body: {}
}
