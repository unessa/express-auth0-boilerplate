// @flow

import {
  startServer,
  stopServer,
  generateTestJWTToken,
  untokenizedApiRequest,
  tokenizedApiRequest,
  expectApiResponse,
  unauthorizedErrorResponse,
  fakeUserTokenWhitelistRedis,
  type TestJWTToken
} from '../../../../test-utils'

import {
  LogoutSuccessResponse
} from './logout.mock'

import {
  isTokenWhitelisted,
  getUserWhitelistedTokens
} from '../../../../utils/user-token-whitelist'

import { API_PATH_AUTH_LOGOUT } from '../.'

async function expectWhitelistedTokensCount(token: TestJWTToken, count: number): Promise<void> {
  const userWhitelistedTokens = await getUserWhitelistedTokens(fakeUserTokenWhitelistRedis, token.payload.sub)
  expect(userWhitelistedTokens.length).toEqual(count)
}

describe('Logout tests', () => {
  beforeAll(startServer)
  afterAll(stopServer)

  it('GET 401 - this path needs authentication', async () => {
    const res = await untokenizedApiRequest(API_PATH_AUTH_LOGOUT).get()
    expectApiResponse(res, unauthorizedErrorResponse)
  })

  it('GET 200 - should revoke user token after logout', async () => {
    const token = generateTestJWTToken(60, true)
    const res = await tokenizedApiRequest(API_PATH_AUTH_LOGOUT, token.token).get()
    const isWhitelisthed = await isTokenWhitelisted(fakeUserTokenWhitelistRedis, token.payload)
    expect(isWhitelisthed).toBeFalsy()
    expectApiResponse(res, LogoutSuccessResponse)
  })

  it('GET 200 - should revoke all user tokens after logout', async () => {
    generateTestJWTToken(60, true)
    const token: TestJWTToken = generateTestJWTToken(60, true)
    await expectWhitelistedTokensCount(token, 2)
    const res = await tokenizedApiRequest(`${API_PATH_AUTH_LOGOUT}?all=true`, token.token).get()
    expectApiResponse(res, LogoutSuccessResponse)
    await expectWhitelistedTokensCount(token, 0)
  })
})
