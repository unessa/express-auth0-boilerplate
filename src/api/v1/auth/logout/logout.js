// @flow

import { removeWhitelistedToken, removeUserTokens } from '../../../../utils/user-token-whitelist'

import type { $Response, NextFunction } from 'express'
import type { ServerConfig } from '../../../../config'
import type { DecodedAuthToken } from '../../../../utils/jwt-decode'
import type { RedisClient } from '../../../../utils/redis-client'

/**
 * @api {get} /api/v1/auth/logout
 * @apiParam {Boolean} [all] Logout from all devices
 * @apiDescription Logout user from server
 * @apiName Logout
 * @apiGroup Auth
 * @apiVersion 1.0.0
 *
 * @apiSuccess {Object} 200 OK
 *
 * @apiExample {curl} curl
 *   curl http://localhost:3000/api/v1/auth/logout
 */
export function logoutRequest(
  config: ServerConfig,
  userTokenWhitelistRedis: RedisClient
) {
  return async (req: any, res: $Response, next: NextFunction) => {
    try {
      const user: DecodedAuthToken = req.user

      if (req.query.all && req.query.all === 'true') {
        await removeUserTokens(userTokenWhitelistRedis, user)
      } else {
        await removeWhitelistedToken(userTokenWhitelistRedis, user)
      }

      res.status(200).send({})
    } catch (e) {
      next(e)
    }
  }
}
