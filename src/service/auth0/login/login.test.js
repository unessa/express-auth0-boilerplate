// @flow

import config from '../../../config'

import {
  startServer,
  stopServer,
  generateTestJWTToken,
  TEST_USER_EMAIL,
  TEST_USER_PASSWORD,
  type TestJWTToken,
  type MockResponse
} from '../../../test-utils'

import {
  createAuth0LoginRequest
} from './login'

import {
  mockAuth0LoginSuccessRequest,
  mockAuth0LoginInvalidUserPasswordRequest
} from './login.mock'

describe('Auth0 - User login', () => {
  beforeAll(startServer)
  afterAll(stopServer)

  it('POST 200 - successfully authenticates user', async () => {
    const testToken: TestJWTToken = generateTestJWTToken()
    const auth0Response = mockAuth0LoginSuccessRequest(testToken)
    const response = await createAuth0LoginRequest(config, TEST_USER_EMAIL, TEST_USER_PASSWORD)
    expect(response).toEqual(auth0Response.body)
  })

  it('POST 401 - Invalid username or password', async () => {
    // FIXME 18.04.2017 nviik - add utility function createEmptyMockResponse
    let auth0Response: MockResponse = { status: 200, body: {} }
    try {
      const testToken: TestJWTToken = generateTestJWTToken()
      auth0Response = mockAuth0LoginInvalidUserPasswordRequest(testToken)
      await createAuth0LoginRequest(config, TEST_USER_EMAIL, TEST_USER_PASSWORD)
    } catch (e) {
      // TODO 18.04.2017 nviik - Maybe add add utility function expectServiceResponse
      expect(e.error).toEqual(auth0Response.body)
      expect(e.statusCode).toEqual(auth0Response.status)
    }
  })
})
