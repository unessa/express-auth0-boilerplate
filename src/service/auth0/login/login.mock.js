// @flow

import config from '../../../config'

import {
  AUTH0_LOGIN_PATH,
  type Auth0LoginRequest,
  type Auth0LoginResponse
} from './login'

import {
  mockServiceRequest,
  TEST_USER_EMAIL,
  TEST_USER_PASSWORD,
  type TestJWTToken,
  type MockResponse
} from '../../../test-utils'

const auth0LoginRequest: Auth0LoginRequest = {
  client_id: config.auth0.clientId,
  username: TEST_USER_EMAIL,
  password: TEST_USER_PASSWORD,
  connection: config.auth0.login.connection,
  grant_type: config.auth0.login.grantType,
  scope: config.auth0.login.scope
}

export function mockAuth0LoginSuccessRequest(testToken: TestJWTToken): MockResponse {
  const auth0LoginResponseBody: Auth0LoginResponse = {
    id_token: testToken.token,
    access_token: 'test-auth0-access-token',
    token_type: 'Bearer'
  }

  const auth0LoginSuccessResponse: MockResponse = {
    status: 200,
    body: auth0LoginResponseBody
  }

  mockServiceRequest(config.auth0.uri, AUTH0_LOGIN_PATH).post(auth0LoginRequest, auth0LoginSuccessResponse)
  return auth0LoginSuccessResponse
}

export function mockAuth0LoginInvalidUserPasswordRequest(testToken: TestJWTToken): MockResponse {
  const auth0LoginInvalidUserPasswordResponse: MockResponse = {
    status: 401,
    body: {
      error: 'invalid_user_password',
      error_description: 'Wrong email or password.'
    }
  }

  mockServiceRequest(config.auth0.uri, AUTH0_LOGIN_PATH).post(auth0LoginRequest, auth0LoginInvalidUserPasswordResponse)
  return auth0LoginInvalidUserPasswordResponse
}
