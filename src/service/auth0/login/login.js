// @flow

import { externalRequest } from '../../../utils/external-request'
import type { ServerConfig } from '../../../config'

export type Auth0LoginRequest = {
  client_id: string,
  username: string,
  password: string,
  connection: string,
  grant_type: string,
  scope: string
}

export type Auth0LoginResponse = {
  id_token: string,
  access_token: string,
  token_type: string
}

export const AUTH0_LOGIN_PATH = '/oauth/ro'

export async function createAuth0LoginRequest(
  config: ServerConfig,
  username: string,
  password: string
): Promise<Auth0LoginResponse> {
  const loginUrl: string = `${config.auth0.uri}${AUTH0_LOGIN_PATH}`
  const loginData: Auth0LoginRequest = {
    client_id: config.auth0.clientId,
    username: username,
    password: password,
    connection: config.auth0.login.connection,
    grant_type: config.auth0.login.grantType,
    scope: config.auth0.login.scope
  }

  return externalRequest('POST', loginUrl, loginData)
}
