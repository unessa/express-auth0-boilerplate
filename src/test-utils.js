// @flow

import bluebird from 'bluebird'
import fakeredis from 'fakeredis'
import supertest from 'supertest'
import nock from 'nock'
import jsonwebtoken from 'jsonwebtoken'
import uuid from 'uuid'
import moment from 'moment'

import config from './config'
import { createServer, shutdownServer } from './server'
import { whitelistUserToken } from './utils/user-token-whitelist'
import { flushRedisClients, type RedisClient } from './utils/redis-client'

import type { DecodedAuthToken } from './utils/jwt-decode'

let server: ?Server = null

export const TEST_USER_EMAIL = 'test@user.com'
export const TEST_USER_PASSWORD = 'test-password'

export type MockResponse = {
  status: number,
  body: Object
}

export type TestJWTToken = {
  payload: DecodedAuthToken,
  token: string
}

export const unauthorizedErrorResponse: MockResponse = {
  status: 401,
  body: {
    error: 'UnauthorizedError',
    message: 'No authorization token was found'
  }
}

export function expectApiResponse(res: Object, expectedResponse: MockResponse): void {
  expect(res.body).toEqual(expectedResponse.body)
  expect(res.status).toEqual(expectedResponse.status)
}

export function untokenizedApiRequest(apiPath: string) {
  return {
    get: async () => {
      return supertest(server)
        .get(apiPath)
    },
    post: async (body: any) => {
      return supertest(server)
        .post(apiPath)
        .send(body)
    }
  }
}

export function tokenizedApiRequest(apiPath: string, jwtToken: string) {
  return {
    get: async () => {
      return supertest(server)
        .get(apiPath)
        .set('Authorization', `Bearer ${jwtToken}`)
    }
  }
}

export function mockServiceRequest(url: string, path: string) {
  return {
    get: (response: MockResponse) => {
      nock(url).get(path).reply(response.status, response.body)
    },
    post: (body: Object, response: MockResponse) => {
      nock(url).post(path, body).reply(response.status, response.body)
    }
  }
}

export const fakeUserTokenWhitelistRedis: RedisClient = bluebird.promisifyAll(fakeredis.createClient(
  config.userTokenWhitelistRedis.host,
  config.userTokenWhitelistRedis.port
))

export const fakeApicacheRedis: RedisClient = bluebird.promisifyAll(fakeredis.createClient(
  config.apicache.redis.host,
  config.apicache.redis.port
))

export function generateTestJWTToken(
  expiresInSeconds: number = 60,
  whitelistToken: boolean = false
): TestJWTToken {
  const expiresTimestamp: number = moment().utc().add(expiresInSeconds, 'seconds').unix()
  const auth0UserId = 'auth0|4doqwierq344weq'

  const payload:DecodedAuthToken = {
    jti: uuid.v4(),
    iss: config.auth0.issuer,
    sub: auth0UserId,
    aud: config.auth0.clientId,
    exp: expiresTimestamp,
    iat: expiresTimestamp
  }

  const token:string = jsonwebtoken.sign(payload, config.auth0.clientSecret, {
    algorithm: config.auth0.algorithm
  })

  if (whitelistToken) {
    whitelistUserToken(fakeUserTokenWhitelistRedis, payload)
  }

  return {
    payload,
    token
  }
}

export async function startServer(): Promise<void> {
  server = await createServer(config, fakeUserTokenWhitelistRedis, fakeApicacheRedis)
}

export async function stopServer(): Promise<void> {
  await flushRedisClients([fakeUserTokenWhitelistRedis])

  if (server) {
    await shutdownServer(server)
    server = null
  }
}

export function getServerInstance() {
  return server
}
