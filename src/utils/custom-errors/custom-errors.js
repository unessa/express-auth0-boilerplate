// @flow

export const API_ERROR_NAME = 'ApiError'

export class ApiError extends Error {
  error: string
  statusCode: number = 500

  constructor(statusCode: number, error: string, message: string) {
    super(message)
    this.name = API_ERROR_NAME
    this.statusCode = statusCode
    this.error = error
  }
}
