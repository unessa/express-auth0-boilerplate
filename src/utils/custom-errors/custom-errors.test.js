// @flow

import { ApiError, API_ERROR_NAME } from './custom-errors'

const TEST_ERROR_NAME = 'TestError'
const TEST_ERROR_MESSAGE = 'test message'

describe('Custom errors', () => {
  it('ApiError', () => {
    const error = new ApiError(400, TEST_ERROR_NAME, TEST_ERROR_MESSAGE)

    expect(error.statusCode).toEqual(400)
    expect(error.error).toEqual(TEST_ERROR_NAME)
    expect(error.message).toEqual(TEST_ERROR_MESSAGE)
    expect(error.name).toEqual(API_ERROR_NAME)
    expect(error instanceof ApiError).toBe(true)
    expect(error.stack).toMatch(/ApiError: test message/)
  })
})
