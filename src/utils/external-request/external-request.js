// @flow

import request from 'request-promise'
import winston from 'winston'

export async function externalRequest(
  method: 'GET' | 'POST' | 'PUT' | 'DELETE',
  uri: string,
  body?: Object
): Promise<any> {
  try {
    const res = await request({
      json: true,
      method: method,
      uri: uri,
      body: body || undefined
    })

    winston.debug(`externalRequest - ${method} ${uri}`, body, res)
    return res
  } catch (e) {
    winston.error(`externalRequest - ${method} ${uri}\n`, body, '\n', e)
    throw e
  }
}
