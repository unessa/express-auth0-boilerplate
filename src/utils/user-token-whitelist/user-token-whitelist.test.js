// @flow

import { flushRedisClients } from '../redis-client'

import {
  startServer,
  stopServer,
  fakeUserTokenWhitelistRedis,
  generateTestJWTToken,
  type TestJWTToken
} from '../../test-utils'

import {
  whitelistUserToken,
  getUserWhitelistedTokens,
  removeExpiredUserTokens,
  removeWhitelistedToken,
  removeUserTokens,
  isTokenWhitelisted
} from './user-token-whitelist'

describe('User token whitelist', () => {
  beforeEach(async () => { await flushRedisClients([fakeUserTokenWhitelistRedis]) })
  beforeAll(startServer)
  afterAll(stopServer)

  it('should whitelist given token', async () => {
    const testToken: TestJWTToken = generateTestJWTToken()
    await whitelistUserToken(fakeUserTokenWhitelistRedis, testToken.payload)
    const tokens = await getUserWhitelistedTokens(fakeUserTokenWhitelistRedis, testToken.payload.sub)
    expect(tokens).toEqual([testToken.payload.jti])
  })

  it('should remove expired tokens', async () => {
    const testToken: TestJWTToken = generateTestJWTToken(0)
    await whitelistUserToken(fakeUserTokenWhitelistRedis, testToken.payload)
    await removeExpiredUserTokens(fakeUserTokenWhitelistRedis, testToken.payload.sub)
    const tokens = await getUserWhitelistedTokens(fakeUserTokenWhitelistRedis, testToken.payload.sub)
    expect(tokens).toEqual([])
  })

  it('should remove token from whitelist', async () => {
    const testToken1: TestJWTToken = generateTestJWTToken()
    const testToken2: TestJWTToken = generateTestJWTToken()
    await whitelistUserToken(fakeUserTokenWhitelistRedis, testToken1.payload)
    await whitelistUserToken(fakeUserTokenWhitelistRedis, testToken2.payload)
    await removeWhitelistedToken(fakeUserTokenWhitelistRedis, testToken1.payload)
    const tokens = await getUserWhitelistedTokens(fakeUserTokenWhitelistRedis, testToken1.payload.sub)
    expect(tokens).toEqual([testToken2.payload.jti])
  })

  it('should remove all user tokens', async () => {
    const testToken1: TestJWTToken = generateTestJWTToken()
    const testToken2: TestJWTToken = generateTestJWTToken()
    await whitelistUserToken(fakeUserTokenWhitelistRedis, testToken1.payload)
    await whitelistUserToken(fakeUserTokenWhitelistRedis, testToken2.payload)
    let tokens = await getUserWhitelistedTokens(fakeUserTokenWhitelistRedis, testToken1.payload.sub)
    expect(tokens.length).toEqual(2)
    await removeUserTokens(fakeUserTokenWhitelistRedis, testToken1.payload)
    tokens = await getUserWhitelistedTokens(fakeUserTokenWhitelistRedis, testToken1.payload.sub)
    expect(tokens).toEqual([])
  })

  describe('check is token whitelisted', () => {
    it('token is whitelisted', async () => {
      const testToken: TestJWTToken = generateTestJWTToken()
      const isWhitelisted = await isTokenWhitelisted(fakeUserTokenWhitelistRedis, testToken.payload)
      expect(isWhitelisted).toBeFalsy()
    })

    it('token is not whitelisted', async () => {
      const testToken: TestJWTToken = generateTestJWTToken()
      await whitelistUserToken(fakeUserTokenWhitelistRedis, testToken.payload)
      const isWhitelisted = await isTokenWhitelisted(fakeUserTokenWhitelistRedis, testToken.payload)
      expect(isWhitelisted).toBeTruthy()
    })
  })
})
