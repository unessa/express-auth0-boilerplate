// @flow

import moment from 'moment'

import type { DecodedAuthToken } from '../jwt-decode'
import type { RedisClient } from '../redis-client'

export async function removeExpiredUserTokens(
  redisClient: RedisClient,
  userId: string
): Promise<void> {
  const now = moment().utc().unix()
  await redisClient.zremrangebyscoreAsync(userId, 0, now)
}

export async function whitelistUserToken(
  redisClient: RedisClient,
  authToken: DecodedAuthToken
): Promise<void> {
  await redisClient.zaddAsync([authToken.sub, authToken.exp, authToken.jti])
}

export async function getUserWhitelistedTokens(
  redisClient: RedisClient,
  userId: string
): Promise<Array<string>> {
  return redisClient.zrangeAsync(userId, 0, -1)
}

export async function removeWhitelistedToken(
  redisClient: RedisClient,
  authToken: DecodedAuthToken
): Promise<void> {
  await redisClient.zremAsync([authToken.sub, authToken.jti])
}

export async function removeUserTokens(
  redisClient: RedisClient,
  authToken: DecodedAuthToken
): Promise<void> {
  await redisClient.delAsync(authToken.sub)
}

export async function isTokenWhitelisted(
  redisClient: RedisClient,
  authToken: DecodedAuthToken
): Promise<boolean> {
  const tokens = await getUserWhitelistedTokens(redisClient, authToken.sub)
  return tokens.includes(authToken.jti)
}
