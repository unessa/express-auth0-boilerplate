// @flow

import JsonSchema from 'jsonschema'
import { ApiError } from '../custom-errors'

const validator = new JsonSchema.Validator()

export const ERROR_SCHEMA_VALIDATION = 'SchemaValidationError'

export function validateJsonSchema(
  data: Object,
  schema: JsonSchema.JsonSchema
): void {
  try {
    return validator.validate(data, schema, {
      throwError: true
    })
  } catch (e) {
    const message = e.stack.replace('instance.', '')
    throw new ApiError(400, ERROR_SCHEMA_VALIDATION, message)
  }
}
