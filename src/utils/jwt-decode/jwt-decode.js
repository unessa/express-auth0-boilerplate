// @flow

import jsonwebtoken from 'jsonwebtoken'

export type DecodedAuthToken = {
  jti: string,
  iss: string,
  sub: string,
  aud: string,
  exp: number,
  iat: number
}

export function decodeAuthToken(
  jwtToken: string,
  secret: string
): DecodedAuthToken {
  try {
    return jsonwebtoken.verify(jwtToken, secret)
  } catch (e) {
    throw new Error('Unable to decode jwt-token')
  }
}
