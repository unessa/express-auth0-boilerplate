// @flow

import config from '../../config'
import { generateTestJWTToken, type TestJWTToken } from '../../test-utils'
import { decodeAuthToken } from './jwt-decode'

describe('JWT decode', () => {
  it('should decode jwt token', () => {
    const testToken:TestJWTToken = generateTestJWTToken()
    const decodedToken = decodeAuthToken(testToken.token, config.auth0.clientSecret)
    expect(decodedToken).toEqual(testToken.payload)
  })

  it('should fail if invalid secret', () => {
    try {
      const testToken: TestJWTToken = generateTestJWTToken()
      decodeAuthToken(testToken.token, 'invalid-secret')
    } catch (e) {
      expect(e.name).toEqual('Error')
      expect(e.message).toEqual('Unable to decode jwt-token')
    }
  })

  it('should fail if invalid jwt token', () => {
    try {
      decodeAuthToken('invalid-token', config.auth0.clientSecret)
    } catch (e) {
      expect(e.name).toEqual('Error')
      expect(e.message).toEqual('Unable to decode jwt-token')
    }
  })
})
