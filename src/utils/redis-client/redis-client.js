// @flow

import bluebird from 'bluebird'
import redis from 'redis'
import winston from 'winston'

import type { RedisClientConfig } from '../../config'

// NOTE 18.04.2017 nviik - Replace this custom type when redis has flow-type support
export type RedisClient = any

function createLogMessage(name: string, message?: string): string {
  const msg = (message !== undefined) ? message : ''
  return `RedisClient (${name}) - ${msg}`
}

export function createRedisClient(
  config: RedisClientConfig
): RedisClient {
  const redisClient: RedisClient = bluebird.promisifyAll(redis.createClient({
    host: config.host,
    port: config.port,
    db: config.db
  }))

  redisClient
    .on('ready', () => {
      winston.info(createLogMessage(config.name, 'connection established'))
    })
    .on('reconnecting', () => {
      winston.info(createLogMessage(config.name, 'reconnecting...'))
    })
    .on('end', () => {
      winston.info(createLogMessage(config.name, 'disconnected'))
    })
    .on('error', (err) => {
      winston.error(createLogMessage(config.name), err)
    })

  return redisClient
}

export function shutdownRedisClients(clients: Array<RedisClient>): void {
  clients.forEach((client) => client.quit())
}

export async function flushRedisClients(clients: Array<RedisClient>): Promise<void> {
  clients.forEach(async (client) => client.flushdbAsync())
}
