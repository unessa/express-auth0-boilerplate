// @flow

import winston from 'winston'

import { API_ERROR_NAME } from '../../utils/custom-errors'

import type { $Application, $Request, $Response, NextFunction } from 'express'

function errorHandlerMiddleware(
  err: any,
  req: $Request,
  res: $Response,
  next: NextFunction
) {
  const statusCode: number = err.status || err.statusCode
  winston.error(`${statusCode} - ${err.name} - ${err.message}`, err.stack)

  switch (err.name) {
    case API_ERROR_NAME:
      return res.status(statusCode).send({
        error: err.error,
        message: err.message
      })

    case 'UnauthorizedError':
      return res.status(statusCode).send({
        error: err.name,
        message: err.message
      })

    default:
      return res.status(statusCode || 500).send({
        error: err.error || 'Internal server error',
        message: err.message
      })
  }
}

export function configureErrorFormatter(app: $Application) {
  app.use(errorHandlerMiddleware)
}
