// @flow

import jwt from 'express-jwt'

import { isTokenWhitelisted } from '../../utils/user-token-whitelist'

import type { $Application, $Request } from 'express'
import type { ServerConfig } from '../../config'
import type { DecodedAuthToken } from '../../utils/jwt-decode'
import type { RedisClient } from '../../utils/redis-client'

async function isTokenRevoked(
  userTokenWhitelistRedis: RedisClient,
  req: $Request,
  payload: DecodedAuthToken,
  done: Function
): Promise<Function> {
  const isWhitelisted: boolean = await isTokenWhitelisted(userTokenWhitelistRedis, payload)
  return done(null, !isWhitelisted)
}

export async function configureApiAuthentication(
  app: $Application,
  config: ServerConfig,
  ignoredPaths: Array<string>,
  userTokenWhitelistRedis: RedisClient
): Promise<void> {
  app.use(
    jwt({
      secret: config.auth0.clientSecret,
      audience: config.auth0.clientId,
      issuer: config.auth0.issuer,
      algorithms: [config.auth0.algorithm],
      isRevoked: (req, payload, done) => isTokenRevoked(userTokenWhitelistRedis, req, payload, done)
    })
    .unless({
      path: ignoredPaths
    })
  )
}
