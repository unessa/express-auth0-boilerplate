// @flow

import type { MockResponse } from '../../test-utils'

export const JwtMalformedResponse: MockResponse =  {
  status: 401,
  body: {
    error: 'UnauthorizedError',
    message: 'jwt malformed'
  }
}
