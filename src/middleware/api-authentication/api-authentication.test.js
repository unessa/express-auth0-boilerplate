// @flow

import {
  JwtMalformedResponse
} from './api-authentication.mock'

import {
  startServer,
  stopServer,
  tokenizedApiRequest,
  expectApiResponse
} from '../../test-utils'

import { API_PATH_AUTH_LOGOUT } from '../../api/v1/auth'

describe('Api authentication', () => {
  beforeEach(startServer)
  afterEach(stopServer)

  it('GET 401 - fails if jwt token is malformed', async () => {
    const malformedToken = 'malformed-token'
    const res = await tokenizedApiRequest(API_PATH_AUTH_LOGOUT, malformedToken).get()
    expectApiResponse(res, JwtMalformedResponse)
  })
})
