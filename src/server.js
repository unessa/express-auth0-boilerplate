// @flow

import express from 'express'
import bodyParser from 'body-parser'
import winston from 'winston'
import expressWinston from 'express-winston'

import { configureApiRoutes } from './api'
import { configureErrorFormatter } from './middleware/error-formatter'

import type { $Application } from 'express'
import type { ServerConfig } from './config'
import type { RedisClient } from './utils/redis-client'

export async function createServer(
  config: ServerConfig,
  userTokenWhitelistRedis: RedisClient,
  apicacheRedis: RedisClient
): Promise<Server> {
  const app: $Application = express()
  app.use(bodyParser.json())

  configureWinston(app, config)
  await configureApiRoutes(app, config, userTokenWhitelistRedis, apicacheRedis)
  configureErrorFormatter(app)

  const server: Server = app.listen(config.port)

  winston.info(`Server listening on port ${config.port}`)
  winston.debug('Using configuration', config)

  return server
}

function configureWinston(app: $Application, config: ServerConfig) {
  const consoleLogConfig = new winston.transports.Console({
    level: config.logLevel,
    json: false,
    colorize: true
  })

  const fileLogConfig = new winston.transports.File({
    filename: config.logFilePath,
    level: config.logLevel
  })

  winston.configure({
    transports: [
      consoleLogConfig,
      fileLogConfig
    ]
  })

  app.use(expressWinston.logger({
    transports: [
      consoleLogConfig,
      fileLogConfig
    ],
    expressFormat: true,
    colorize: true,
    meta: false,
    msg: 'HTTP {{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}} {{res.body}}'
  }))
}

export function shutdownServer(server: Server): void {
  winston.info('Shutting server down...')
  server.close()
}
