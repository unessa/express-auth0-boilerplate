// @flow

import fs from 'fs'
import path from 'path'
import winston from 'winston'

import defaultConfig from './config-default'
import testConfig from './config-test'

export type RedisClientConfig = {
  name: string,
  host: string,
  port: number,
  db: number
}

export type ServerConfig = {
  port: number,
  logFilePath: string,
  logLevel: string,
  auth0: {
    clientId: string,
    clientSecret: string,
    issuer: string,
    audience: string,
    uri: string,
    algorithm: string,
    login: {
      connection: string,
      grantType: string,
      scope: string
    }
  },
  userTokenWhitelistRedis: RedisClientConfig,
  apicache: {
    enabled: boolean,
    expires: string,
    includedStatusCodes: Array<number>,
    redis: RedisClientConfig
  },
  omdbApiUri: string
}

const LOCAL_CONFIG_FILE_NAME = 'config-local.js'

function importLocalConfig(): ServerConfig {
  const localConfigFilePath = path.resolve(__dirname, LOCAL_CONFIG_FILE_NAME)

  if (fs.existsSync(localConfigFilePath)) {
    // $FlowFixMe NOTE 13.04.2017 nviik - Suppress file not found flow error
    return require(`./${LOCAL_CONFIG_FILE_NAME}`).default
  }

  if (!process.env.NODE_ENV || (process.env.NODE_ENV === 'development')) {
    winston.error('Local config not found. To run application in development mode create local config.')
  }

  return defaultConfig
}

function getActiveConfig(): ServerConfig {
  const envName = process.env.NODE_ENV

  switch (envName) {
    case 'test':
      return testConfig

    default:
      return importLocalConfig()
  }
}

const activeConfig: ServerConfig = getActiveConfig()
export default activeConfig
