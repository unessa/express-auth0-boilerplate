// @flow

import _ from 'lodash'
import defaultConfig from './config-default'

import type { ServerConfig } from '.'

const testConfig: ServerConfig = _.merge({}, defaultConfig, {
  logLevel: 'none',
  auth0: {
    clientId: 'test-client-id',
    clientSecret: 'test-client-secret',
    issuer: 'test-issuer',
    audience: 'test-audience',
    uri: 'https://test-auth.server'
  }
})

export default testConfig
