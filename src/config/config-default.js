// @flow

import type { ServerConfig, RedisClientConfig } from '.'

const userTokenWhitelistRedis: RedisClientConfig = {
  name: 'user-token-whitelist',
  host: '127.0.0.1',
  port: 6379,
  db: 0
}

const apicacheRedis: RedisClientConfig = {
  name: 'apicache',
  host: '127.0.0.1',
  port: 6379,
  db: 1
}

const defaultConfig: ServerConfig = {
  port: 3000,
  logFilePath: 'debug.log',
  logLevel: 'debug',
  auth0: {
    clientId: '',
    clientSecret: '',
    issuer: '',
    audience: '',
    uri: '',
    algorithm: 'HS256',
    login: {
      connection: 'Username-Password-Authentication',
      grantType: 'password',
      scope: 'openid jti'
    }
  },
  userTokenWhitelistRedis,
  apicache: {
    enabled: true,
    expires: '120 seconds',
    includedStatusCodes: [200],
    redis: apicacheRedis
  },
  omdbApiUri: 'http://www.omdbapi.com'
}

export default defaultConfig
