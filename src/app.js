// @flow

import 'babel-polyfill'

import config from './config'
import { createServer, shutdownServer } from './server'
import { createRedisClient, shutdownRedisClients } from './utils/redis-client'

const userTokenWhitelistRedis = createRedisClient(config.userTokenWhitelistRedis)
const apicacheRedis = createRedisClient(config.apicache.redis)

const server = createServer(config, userTokenWhitelistRedis, apicacheRedis)

server.then((instance) => {
  process.on('SIGINT', async () => {
    shutdownServer(instance)
    shutdownRedisClients([userTokenWhitelistRedis, apicacheRedis])
  })
})
