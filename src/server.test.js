// @flow

import {
  startServer,
  stopServer,
  getServerInstance
} from './test-utils'

describe('Server test', () => {
  afterAll(stopServer)

  // TODO 18.04.2017 nviik - This test is not testing server functionality
  //    after refactoring server to be stateless. Figure out how this
  //    should be testesd.
  it('should start and stop server correctly', async () => {
    expect(getServerInstance()).toBeNull()
    await startServer()
    expect(getServerInstance()).not.toBeNull()
    await stopServer()
    expect(getServerInstance()).toBeNull()
  })
})
