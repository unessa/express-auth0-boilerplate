# Express auth0 server boilerplate

This boilerplate is used to create simple node server API with
user authentication. Auth0 is used for authentication.

To start in development mode run `npm intall && npm start`
To build release version `npm run build && node ./build/app.js`
To update flowtype run `./node_modules/.bin/flow-typed update`

# Auth0 configuration

## Section: Clients
* Create new non-interactive client (don't apply any auth0 API's to this endpoint)
* Check that JWT token signing method to HS256 (client settings -> advanced -> OAuth)

## Section: connections
* Create new database connection
* This connection is used for in config.auth0.connection

## Section: Rules
You need to add new rule for JTI, which is defined like This

```
function (user, context, callback) {
  user.jti = require('uuid').v4();
  callback(null, user, context);
}
```
