// flow-typed signature: 697014530f6f43e69c8ba43e8fc14a2e
// flow-typed version: <<STUB>>/babel-plugin-transform-builtin-extend_v^1.1.2/flow_v0.43.1

/**
 * This is an autogenerated libdef stub for:
 *
 *   'babel-plugin-transform-builtin-extend'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the 
 * community by sending a pull request to: 
 * https://github.com/flowtype/flow-typed
 */

declare module 'babel-plugin-transform-builtin-extend' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module 'babel-plugin-transform-builtin-extend/lib/index' {
  declare module.exports: any;
}

declare module 'babel-plugin-transform-builtin-extend/test/approximate/index' {
  declare module.exports: any;
}

declare module 'babel-plugin-transform-builtin-extend/test/standard/index' {
  declare module.exports: any;
}

// Filename aliases
declare module 'babel-plugin-transform-builtin-extend/lib/index.js' {
  declare module.exports: $Exports<'babel-plugin-transform-builtin-extend/lib/index'>;
}
declare module 'babel-plugin-transform-builtin-extend/test/approximate/index.js' {
  declare module.exports: $Exports<'babel-plugin-transform-builtin-extend/test/approximate/index'>;
}
declare module 'babel-plugin-transform-builtin-extend/test/standard/index.js' {
  declare module.exports: $Exports<'babel-plugin-transform-builtin-extend/test/standard/index'>;
}
