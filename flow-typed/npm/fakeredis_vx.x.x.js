// flow-typed signature: 75a73a8a2c6b5e5f1eeaac7b20878a5d
// flow-typed version: <<STUB>>/fakeredis_v^2.0.0/flow_v0.43.1

/**
 * This is an autogenerated libdef stub for:
 *
 *   'fakeredis'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the 
 * community by sending a pull request to: 
 * https://github.com/flowtype/flow-typed
 */

declare module 'fakeredis' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module 'fakeredis/lib/backend' {
  declare module.exports: any;
}

declare module 'fakeredis/lib/connection' {
  declare module.exports: any;
}

declare module 'fakeredis/lib/helpers' {
  declare module.exports: any;
}

declare module 'fakeredis/main' {
  declare module.exports: any;
}

declare module 'fakeredis/redis.test' {
  declare module.exports: any;
}

declare module 'fakeredis/test' {
  declare module.exports: any;
}

// Filename aliases
declare module 'fakeredis/lib/backend.js' {
  declare module.exports: $Exports<'fakeredis/lib/backend'>;
}
declare module 'fakeredis/lib/connection.js' {
  declare module.exports: $Exports<'fakeredis/lib/connection'>;
}
declare module 'fakeredis/lib/helpers.js' {
  declare module.exports: $Exports<'fakeredis/lib/helpers'>;
}
declare module 'fakeredis/main.js' {
  declare module.exports: $Exports<'fakeredis/main'>;
}
declare module 'fakeredis/redis.test.js' {
  declare module.exports: $Exports<'fakeredis/redis.test'>;
}
declare module 'fakeredis/test.js' {
  declare module.exports: $Exports<'fakeredis/test'>;
}
